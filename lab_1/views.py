from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Nadhif Adyatma Prayoga'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 9, 23) 
npm = 1806205501
angkatan = 2018
hobi1 = "Browse the Internet"
hobi2 = "Play Rhythm Games"
hobi3 = "Watch Anime"
desc = "A particular human that is interested in Computer Science"
tempat_kuliah = "Universitas Indonesia"


# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'angkatan': angkatan, 'hobi1': hobi1, 'hobi2': hobi2, 
    'hobi3': hobi3, "description": desc, "tempat_kuliah": tempat_kuliah, 'angkatan': angkatan}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
